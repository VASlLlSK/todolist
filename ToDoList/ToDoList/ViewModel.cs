﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Dapper;
using MySql.Data.MySqlClient;

namespace ToDoList
{
    class ViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Task> Tasks { get; set; }

        public ViewModel()
        {
            DB db = new DB();
            db.OpenConnection();
            var tasks = db.GetAllTasks();
            db.CloseConnection();
            Tasks = new ObservableCollection<Task>();
            foreach (var task in tasks)
            {
                Tasks.Add(task);
            }
        }

        public Task editTask { get; set; }

        private RelayCommandwith _dontSaveTaskCommand;
        public RelayCommandwith DontSaveTaskCommand
        {
            get
            {
                return _dontSaveTaskCommand ??
                  (_dontSaveTaskCommand = new RelayCommandwith(x =>
                  {

                  }));
            }
        }

        private RelayCommandwith _saveTaskCommand;
        public RelayCommandwith SaveTaskCommand
        {
            get
            {
                return _saveTaskCommand ??
                  (_saveTaskCommand = new RelayCommandwith(x =>
                  {
                      var task = x as Task;
                      task.Title = task.Title.Trim();
                      task.TaskContent = task.TaskContent.Trim();

                      if (task.Title != "")
                      {
                          DB db = new DB();
                          db.OpenConnection();
                          db.EditTask(task);
                          db.CloseConnection();
                      }
                  }));
            }
        }

        private RelayCommandwith _editTaskCommand;
        public RelayCommandwith EditTaskCommand
        {
            get
            {
                return _editTaskCommand ??
                  (_editTaskCommand = new RelayCommandwith(x =>
                  {
                      var task = new Task(x as Task);

                      EditWindow editWindow = new EditWindow(task);
                      editWindow.ShowDialog();

                      if (task.Title != "")
                      {
                          var index = Tasks.IndexOf(x as Task);
                          Tasks.Remove(x as Task);
                          Tasks.Insert(index, task);
                      }
                  }));
            }
        }

        private RelayCommandwith _doneTaskCommand;
        public RelayCommandwith DoneTaskCommand
        {
            get
            {
                return _doneTaskCommand ??
                  (_doneTaskCommand = new RelayCommandwith(x =>
                  {
                      var task = x as Task;
                      DB db = new DB();
                      db.OpenConnection();
                      db.UpdateDoneTask(task);
                      db.CloseConnection();
                  }));
            }
        }

        private RelayCommandwith _deleteTaskCommand;
        public RelayCommandwith DeleteTaskCommand
        {
            get
            {
                return _deleteTaskCommand ??
                  (_deleteTaskCommand = new RelayCommandwith(x =>
                  {
                      var task = x as Task;
                      Tasks.Remove(task);
                      DB db = new DB();
                      db.OpenConnection();
                      db.DeleteTask(task.id);
                      db.CloseConnection();
                  }));
            }
        }

        private RelayCommand _addTaskCommand;
        public RelayCommand AddTaskCommand
        {
            get
            {
                return _addTaskCommand ??
                  (_addTaskCommand = new RelayCommand(() =>
                  {
                      Task task = new Task();

                      EditWindow editWindow = new EditWindow(task);
                      editWindow.ShowDialog();

                      if (task.Title != "")
                      {
                          DB db = new DB();
                          db.OpenConnection();
                          db.CreateTask(task);

                          Tasks.Add(db.GetLastTask());

                          db.CloseConnection();
                      }
                     
                  }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class RelayCommandwith : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommandwith(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }

    public class RelayCommand : ICommand
    {
        private Action execute;
        private Func<bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute();
        }

        public void Execute(object parameter)
        {
            this.execute();
        }
    }

    public class Task
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string TaskContent { get; set; }
        public bool Done { get; set; }
        public Task()
        {

        }
        public Task(Task task)
        {
            this.id = task.id;
            this.Title = task.Title;
            this.TaskContent = task.TaskContent;
            this.Done = task.Done;
        }
    }


    public class DB
    {
        string _connectionString = "Server=localhost;Port=3306;Database=todolist;Uid=root;Pwd=54321ASASAS;";
        private MySqlConnection connection;
        public DB()
        {
            connection = new MySqlConnection(_connectionString);
        }

        public MySqlConnection GetConnection()
        {
            return connection;
        }

        public void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                connection.Open();
        }

        public void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        public List<Task> GetAllTasks()
        {
            List<Task> tasks = new List<Task>();

            var sql = "SELECT * FROM todolist.tasks";

            tasks = connection.Query<Task>(sql).ToList();
            return tasks;
        }

        public void CreateTask(Task task)
        {
            var sql = "INSERT INTO todolist.tasks (id, Title, TaskContent, Done) VALUES (@id, @Title, @TaskContent, @Done)";

            connection.Query<Task>(sql, new { id = task.id, Title = task.Title, TaskContent = task.TaskContent, Done = task.Done });
        }

        public void EditTask(Task task)
        {
            var sql = "UPDATE todolist.tasks SET Title = @Title, TaskContent = @TaskContent WHERE id = @id";
            connection.Query<Task>(sql, new { id = task.id, Title = task.Title, TaskContent = task.TaskContent });
        }

        public void UpdateDoneTask(Task task)
        {
            var sql = "UPDATE todolist.tasks SET Done = @Done WHERE id = @id";
            connection.Query<Task>(sql, new { id = task.id, Done = task.Done });
        }

        public void DeleteTask(int id)
        {
            var sql = "DELETE FROM todolist.tasks WHERE id = @id";
            connection.Query<Task>(sql, new { id });
        }

        public Task FindTask(int id)
        {
            var sql = "SELECT * FROM todolist.tasks WHERE id=@id";

            var task = connection.Query<Task>(sql, new { id }).First();
            return task;
        }

        public Task GetLastTask()
        {
            var sql = "SELECT* FROM todolist.tasks WHERE id = LAST_INSERT_ID();";
            var task = connection.Query<Task>(sql).First();
            return task;
        }
    }
}
